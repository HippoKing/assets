--文件说明:本文件用于提供lua环境初始化时调用的main方法
importEx "BlackJack.BJFramework.Runtime.Lua"

local LuaManagerModule = {
    -- 这里注册一些自动Hotfix的module
    RegHotFix = function(luaManager)
        --luaManager:RegHotFix("BlackJack.ProjectL.UI.LoginUITask", require("GameLogic.LoginUITask"))
		-- luaManager:RegHotFix("BlackJack.ProjectL.UI.ARHeroListUIController", require("GameLogic.ARHeroListUIController"))		
		--luaManager:RegHotFix("BlackJack.ProjectL.UI.BagListUIController", require("GameLogic.BagListUIController"))
    end
}

function main()
	Debug.Log("LuaManagerInit.txt.main enter")
    LuaManagerModule.RegHotFix(LuaManager.Instance)
end
